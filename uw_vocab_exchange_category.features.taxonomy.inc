<?php

/**
 * @file
 * uw_vocab_exchange_category.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function uw_vocab_exchange_category_taxonomy_default_vocabularies() {
  return array(
    'uwaterloo_exchange_category' => array(
      'name' => 'Exchange Category',
      'machine_name' => 'uwaterloo_exchange_category',
      'description' => 'Exchange category for exchange board',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
